# Minimal Docker image for CI using SFDX
Dockerfile for Salesforce DX with minimal configuration.

The main use of this Dockerfile is for continuous integration environments. We could use SFDX prioritizing performance.

You can use this docker from [dacalle/sfdxlite](https://hub.docker.com/r/dacalle/sfdxlite)