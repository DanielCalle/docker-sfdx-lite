FROM node:12-alpine
LABEL maintainer "Daniel Calle <danielcallesanchez@gmail.com>"

# SFDX
ENV SFDX_AUTOUPDATE_DISABLE false
ENV SFDX_USE_GENERIC_UNIX_KEYCHAIN true
ENV SFDX_DOMAIN_RETRY 600
ENV SFDX_LOG_LEVEL DEBUG
RUN npm install sfdx-cli --global && \
    npm cache verify

# OPENSSL
RUN apk upgrade --update-cache --available && \
    apk add --no-cache openssl

RUN mkdir /src
WORKDIR /src
SHELL ["/bin/bash", "-c"]
